@extends('layout')

@section('content')
    <div class="list-group">
        @foreach($cars as $car)
            <a class="list-group-item list-group-item-action" href="/cars/{{ $car->id }}">{{ $car->title }}</a>
        @endforeach
    </div>
@endsection
