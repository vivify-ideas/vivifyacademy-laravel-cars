@extends('layout')

@section('content')
    <div class="card">
        <div class="card-body">
            <p>Title: {{ $car->title }}</p>
            <p>Producer: {{ $car->producer }}<p>
            <p>Number of doors: {{ $car->number_of_doors }}</p>
        </div>
    </div>
@endsection
